package com.swiftmessage.loader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class SwiftMessageLoadInitializer {

	public static void main(String[] args) {
		File folder = new File("swiftmessages");
		File[] files = folder.listFiles();
		List<List<SwiftMessage>> consolidatedSwiftMessagesList = new ArrayList<>();
		List<Future<List<SwiftMessage>>> resultList = new ArrayList<>();
		
		ExecutorService executor = Executors.newFixedThreadPool(4);		
		
		for (int i = 0; i < files.length; i++) {
			SwiftMessageLoader readSwiftMessageFile = new SwiftMessageLoader(files[i].getAbsolutePath());
			Future<List<SwiftMessage>> csvRecords = executor.submit(readSwiftMessageFile);
			resultList.add(csvRecords);
		}
		
		for (int i = 0; i < resultList.size(); i++) {
			Future<List<SwiftMessage>> oneFileRecords = resultList.get(i);
			try {
				List<SwiftMessage> temp = oneFileRecords.get();
				consolidatedSwiftMessagesList.add(temp);
				
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		List<SwiftMessage> flattenedSwiftMessages = consolidatedSwiftMessagesList.stream().flatMap(l->l.stream()).collect(Collectors.toList());
		FilterHighValueMessages filter = new FilterHighValueMessages();
		List<SwiftMessage> highValueSwiftMessages = filter.getHighValueMessages(flattenedSwiftMessages);
		highValueSwiftMessages.stream().forEach(System.out::println);
	}

}
