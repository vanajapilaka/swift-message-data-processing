package com.swiftmessage.loader;

import java.util.List;
import java.util.stream.Collectors;

public class FilterHighValueMessages {
public List<SwiftMessage> getHighValueMessages(List<SwiftMessage> swiftMessages) {
	return swiftMessages.stream().filter(message->(message.getAmount()>500000)).collect(Collectors.toList());
}
}
