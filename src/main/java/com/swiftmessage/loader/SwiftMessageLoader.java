package com.swiftmessage.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.io.InputStreamReader;

public class SwiftMessageLoader implements Callable<List<SwiftMessage>> {

	private String filePath;

	public SwiftMessageLoader(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public List<SwiftMessage> call() throws Exception {
		List<SwiftMessage> swiftMessages = new ArrayList<>();
		File swiftMessagesFile = new File(this.filePath);
		InputStream smStream = new FileInputStream(swiftMessagesFile);
		BufferedReader smReader = new BufferedReader(new InputStreamReader(smStream));
		swiftMessages = smReader.lines().map(splitLines).collect(Collectors.toList());
		return swiftMessages;
	}

	public Function<String, SwiftMessage> splitLines = (line) -> {
		String[] messageParams = line.split(",");
		SwiftMessage sm = new SwiftMessage();
		sm.setMessageId(messageParams[0]);
		sm.setMtType(messageParams[1]);
		sm.setSenderBIC(messageParams[2]);
		sm.setReceiverBIC(messageParams[3]);
		sm.setAmount(Integer.parseInt(messageParams[4]));
		sm.setAccountNo(messageParams[5]);
		sm.setStatus(messageParams[6]);
		return sm;
	};
}
