package com.swiftmessage.loader;

public class SwiftMessage {
	private String messageId;
	private String mtType;
	private String senderBIC;
	private String receiverBIC;
	private int amount;
	private String accountNo;
	private String status;

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMtType() {
		return mtType;
	}

	public void setMtType(String mtType) {
		this.mtType = mtType;
	}

	public String getSenderBIC() {
		return senderBIC;
	}

	public void setSenderBIC(String senderBIC) {
		this.senderBIC = senderBIC;
	}

	public String getReceiverBIC() {
		return receiverBIC;
	}

	public void setReceiverBIC(String receiverBIC) {
		this.receiverBIC = receiverBIC;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SwiftMessage [messageId=" + messageId + ", mtType=" + mtType + ", senderBIC=" + senderBIC
				+ ", receiverBIC=" + receiverBIC + ", amount=" + amount + ", accountNo=" + accountNo + ", status="
				+ status + "]";
	}

}
